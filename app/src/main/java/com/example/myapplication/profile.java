package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.icu.text.Collator;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.bumptech.glide.Glide;
import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class profile extends AppCompatActivity {

    TextView name;
    TextView examCount;
    TextView pastExamCount;
    TextView futureExamCount;
    TextView email, username;
    FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseDatabase database;
    int pastExams, futureExams;
    ImageView profilePic;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        name = (TextView)findViewById(R.id.tv_name);
        examCount = (TextView)findViewById(R.id.tv_examCount);
        pastExamCount = (TextView)findViewById(R.id.tv_pastExamCount);
        futureExamCount = (TextView)findViewById(R.id.tv_futureExamCount);
        profilePic = (ImageView)findViewById(R.id.iv_proflePic);
        username = (TextView)findViewById(R.id.username_field);
        email = (TextView)findViewById(R.id.email_field);

        DatabaseReference firebaseDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(mAuth.getUid());
        firebaseDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                name.setText(dataSnapshot.child("name").getValue().toString());
                examCount.setText(String.valueOf(dataSnapshot.child("Exams").getChildrenCount()));
                for (DataSnapshot postSnapshot: dataSnapshot.child("Exams").getChildren()) // running on all the users exams
                {
                    if(datePassed(postSnapshot.child("date").getValue().toString())) // checking if the exam was in the past or not yet
                        pastExams++;
                    else
                        futureExams++;
                }
                pastExamCount.setText(String.valueOf(pastExams)); // updating fields
                futureExamCount.setText(String.valueOf(futureExams));
                email.setText(user.getEmail());
                username.setText(dataSnapshot.child("name").getValue().toString());
                Glide.with(profile.this).load(user.getPhotoUrl()).into(profilePic);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(profile.this,"Error: couldn't read DataBase!",Toast.LENGTH_SHORT).show();
            }
        });

        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (intent.resolveActivity(getPackageManager()) != null)
                {
                    startActivityForResult(intent, 1910);
                }
            }
        });
    }



    public boolean datePassed(String date){
        try {
            if(new SimpleDateFormat("dd/MM/yyyy").parse(date).before(new Date()))
                return true;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void upload_pic (Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

        final StorageReference store_ref = FirebaseStorage.getInstance().getReference()
                .child("profilePictures").child(FirebaseAuth.getInstance().getCurrentUser().getUid() + ".jpeg");
        store_ref.putBytes(baos.toByteArray()).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                getDownloadUrl(store_ref);
            }
        }).addOnCanceledListener(new OnCanceledListener() {
            @Override
            public void onCanceled() {
                Toast.makeText(profile.this, "Failed to upload profile picture...", Toast.LENGTH_SHORT).show();
            }
        });
    }

    // function gets the url for downloading the photo
    private void getDownloadUrl(StorageReference store_ref)
    {
        store_ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                updateUserProfilePicOnStorage(uri);
            }
        });
    }

    // function updates the profile picture in the firebase storage
    private void updateUserProfilePicOnStorage(Uri uri)
    {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        UserProfileChangeRequest request = new UserProfileChangeRequest.Builder().setPhotoUri(uri).build();
        user.updateProfile(request).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(profile.this, "Profile picture updated!", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(profile.this, "Failed to update profile picture!", Toast.LENGTH_SHORT).show();
            }
        });


    }

    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1910)
        {
            switch (resultCode)
            {
                case RESULT_OK:
                    Bitmap bitmap = (Bitmap)data.getExtras().get("data");
                    profilePic.setImageBitmap(bitmap);
                    upload_pic(bitmap);
                    break;
                default:
                    Toast.makeText(profile.this, "Profile Picture Update Failed!", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }

}
