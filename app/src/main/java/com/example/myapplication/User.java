package com.example.myapplication;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class User {
    private String name;
    private String email;
    private String uid;
    HashMap<String, Exam> exams;

    public void addExam(String name, Exam exam)
    {
        this.exams.put(name, exam);
    }

    public HashMap<String, Exam> getExams() {
        return exams;
    }

    public User()
    {
        this.exams = new HashMap<String, Exam>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public User(String email, String name, String uid) {
        this.email = email;
        this.name = name;
        this.uid = uid;
    }

    public User(User other) {
        this.email = other.email;
        this.name = other.name;
        this.uid = other.uid;
    }

    public void addUserToDB(User user)
    {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("Users");
        mDatabase.child(user.getUid()).setValue(user);
    }

}

