package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class acitivity_dashboard extends AppCompatActivity {

    TextView welcomeMsg;
    ImageView logoutBtn;
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    String welcomeMsgText;
    User tempUser = new User();
    User user;
    DatabaseReference firebaseDatabase;
    TextView createExam_button;
    TextView profile_button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acitivity_dashboard);
        welcomeMsg = (TextView)findViewById(R.id.welcome_textView);
        logoutBtn = (ImageView)findViewById(R.id.logout_button);
        createExam_button = (TextView)findViewById(R.id.createexam_tile);
        profile_button = (TextView)findViewById(R.id.myprofile_tile);

        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.signOut();
                finish();
                onBackPressed();
            }
        });

        firebaseDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(mAuth.getUid());
        firebaseDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                welcomeMsg.setText("Welcome,\n" + dataSnapshot.child("name").getValue().toString());
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(acitivity_dashboard.this,"Error: couldn't read DataBase!",Toast.LENGTH_SHORT).show();
            }
        });

        createExam_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent CreateExamIntent = new Intent(acitivity_dashboard.this, createExam.class);
                startActivity(CreateExamIntent);
            }
        });

        profile_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent profileActivityIntent = new Intent(acitivity_dashboard.this, profile.class);
                startActivity(profileActivityIntent);
            }
        });
    }

}
