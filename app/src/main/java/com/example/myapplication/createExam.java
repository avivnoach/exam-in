package com.example.myapplication;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.audiofx.EnvironmentalReverb;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;

public class createExam extends AppCompatActivity {

    String startTimeStr;
    String endTimeStr;
    String dateStr;
    EditText name;
    EditText examClass;
    EditText comments;
    ImageView examReady;
    TextView dateText;
    TextView endTimeText;
    TextView startTimeText;
    ImageView datePickButton;
    ImageView startTimePickButton;
    ImageView endTimePickButton;
    int day, month, year;
    int endHour, endMin;
    Context mContext = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_exam);
        name = (EditText)findViewById(R.id.exam_name_box);
        examClass = (EditText)findViewById(R.id.exam_class_box);
        comments = (EditText)findViewById(R.id.comments_box);
        datePickButton = (ImageView)findViewById(R.id.set_date_button);
        examReady = (ImageView)findViewById(R.id.exam_ready_arrow);
        dateText = (TextView)findViewById(R.id.date_text);
        startTimeText = (TextView)findViewById(R.id.start_time_text);
        startTimePickButton = (ImageView) findViewById(R.id.set_start_button);
        endTimePickButton = (ImageView)findViewById(R.id.set_end_button);
        endTimeText = (TextView)findViewById(R.id.end_time_text);
        datePickButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog dpd;
                Calendar c= Calendar.getInstance();
                day = c.get(Calendar.DAY_OF_MONTH);
                month = c.get(Calendar.MONTH)+1;
                year = c.get(Calendar.YEAR);
                dpd = new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int mYear, int mMonth, int mDay) {
                        dateStr = "Date: " + mDay + "/" + (mMonth+1) + "/" + mYear;
                        dateText.setText(dateStr);
                    }
                }, year, month, day);
                dpd.show();
            }
        });

        startTimePickButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                int startHour = c.get(Calendar.HOUR_OF_DAY);
                int startMin = c.get(Calendar.MINUTE);
                TimePickerDialog tpd = new TimePickerDialog(mContext, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String strHour = String.valueOf(hourOfDay);
                        String strMin = String.valueOf(minute);
                        if(hourOfDay < 10)
                            strHour = "0" + strHour;
                        if(minute < 10)
                            strMin = "0" + strMin;
                        startTimeStr = "Start Time: " + strHour + ":" + strMin;
                        startTimeText.setText(startTimeStr);
                    }
                }, startHour, startMin, android.text.format.DateFormat.is24HourFormat(mContext));
                tpd.show();
            }
        });

        endTimePickButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                int endHour = c.get(Calendar.HOUR_OF_DAY);
                int endMin = c.get(Calendar.MINUTE);
                TimePickerDialog tpd = new TimePickerDialog(mContext, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String strHour = String.valueOf(hourOfDay);
                        String strMin = String.valueOf(minute);
                        if(hourOfDay < 10)
                            strHour = "0" + strHour;
                        if(minute < 10)
                            strMin = "0" + strMin;
                        endTimeStr = "End Time: " + strHour + ":" + strMin;
                        endTimeText.setText(endTimeStr);
                    }
                }, endHour, endMin, android.text.format.DateFormat.is24HourFormat(mContext));
                tpd.show();
            }
        });

        examReady.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                final String nameStr = name.getText().toString();
                final String classStr = examClass.getText().toString();
                final String commentsStr = comments.getText().toString();
                final String examDateStr = dateText.getText().toString().split(" ")[1]; // before: "Date: dd/mm/yyyy" after: "dd/mm/yyyy"
                final String examStartTime = startTimeText.getText().toString().split(" ")[2]; // same as above
                final String examEndTime = endTimeText.getText().toString().split(" ")[2];
                Exam newExam = new Exam(nameStr, classStr, dateStr, startTimeStr, endTimeStr, commentsStr);
                FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference myRef = database.getReference("Users/"+ currentFirebaseUser.getUid() +"/Exams");
                myRef.child(nameStr).setValue(new Exam(nameStr, classStr, examDateStr, examStartTime, examEndTime, commentsStr));
                Toast.makeText(getApplicationContext(), "Exam saved in Database!", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
