package com.example.myapplication;

public class Exam {
    private String name;
    private String examClass;
    private String date;
    private String startTime;
    private String endTime;
    private String comments;
    private boolean examPassed;
    private int grade;

    public Exam(String name, String examClass, String date, String startTime, String endTime, String comments, boolean examPassed, int grade) {
        this.name = name;
        this.examClass = examClass;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.comments = comments;
        this.examPassed = examPassed;
        if(examPassed)
        {
            this.grade = grade;
        }
        else
        {
            this.grade = -1;
        }
    }

    public Exam(String name, String examClass, String date, String startTime, String endTime, String comments) {
        this.name = name;
        this.examClass = examClass;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.comments = comments;
        this.examPassed = false;
        this.grade = -1;
    }

    public boolean addExamToUserDB(User user)
    {

        return true; // returning always true so i can know if the function failed, (if failed, won't return true)
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExamClass() {
        return examClass;
    }

    public void setExamClass(String examClass) {
        this.examClass = examClass;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public boolean isExamPassed() {
        return examPassed;
    }

    public void setExamPassed(boolean examPassed) {
        this.examPassed = examPassed;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }
}
