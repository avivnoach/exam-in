package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class signup_activity extends AppCompatActivity {

    ImageButton loginButton;

    FirebaseDatabase db;
    DatabaseReference myRef;

    EditText _email;
    EditText _fullname;
    EditText _pass;
    ImageView btn_signup;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_activity);
        FirebaseApp.initializeApp(this);

        db = FirebaseDatabase.getInstance();
        myRef = db.getReference();

        mAuth = FirebaseAuth.getInstance();
        _email = (EditText)findViewById(R.id.email_box);
        _fullname = (EditText)findViewById(R.id.name_box);
        _pass = (EditText)findViewById(R.id.password_box);
        btn_signup = (ImageView) findViewById(R.id.signup_button);
        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = _email.getText().toString();
                final String pwd = _pass.getText().toString();
                final String name = _fullname.getText().toString();
                if(email.isEmpty()){
                    _email.setError("Please enter email id");
                    _email.requestFocus();
                }
                if(pwd.isEmpty()){
                    _pass.setError("Please enter your password");
                    _pass.requestFocus();
                }
                if(name.isEmpty()){
                    _fullname.setError("Please enter full name");
                    _fullname.requestFocus();
                }
                if(!(email.isEmpty() && pwd.isEmpty() && name.isEmpty())){
                    mAuth.createUserWithEmailAndPassword(email, pwd).addOnCompleteListener(signup_activity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(!task.isSuccessful()){
                                Toast.makeText(signup_activity.this,"Sign Up was unsuccessful, Please Try Again",Toast.LENGTH_SHORT).show();
                            }
                            else {
                                Toast.makeText(signup_activity.this,"Welcome, "+name,Toast.LENGTH_SHORT).show();
                                User user = new User(email, name, mAuth.getUid());
                                user.addUserToDB(user);
                                finish();
                                Intent intent = new Intent(signup_activity.this, acitivity_dashboard.class);

                                startActivity(intent);
                            }
                        }
                    });
                }
                else{
                    Toast.makeText(signup_activity.this,"Error Occurred!",Toast.LENGTH_SHORT).show();

                }

            }
        });


        loginButton = findViewById(R.id.signuparrow);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(signup_activity.this, MainActivity.class);
                startActivity(intent);
            }
        });


    }



    // [END on_start_check_user]
}
